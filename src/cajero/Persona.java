/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cajero;

/**
 *
 * @author J107-04
 */
public class Persona {
    
    private int id;
    private String nombre_completo;
    private int nip;
    private String no_tarjeta;
    private double fondos;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre_completo() {
        return nombre_completo;
    }

    public void setNombre_completo(String nombre_completo) {
        this.nombre_completo = nombre_completo;
    }

    public int getNip() {
        return nip;
    }

    public void setNip(int nip) {
        this.nip = nip;
    }

    public String getNo_tarjeta() {
        return no_tarjeta;
    }

    public void setNo_tarjeta(String no_tarjeta) {
        this.no_tarjeta = no_tarjeta;
    }

    public double getFondos() {
        return fondos;
    }

    public void setFondos(double fondos) {
        this.fondos = fondos;
    }

    @Override
    public String toString() {
        return "Persona{" + "id=" + id + ", nombre_completo=" + nombre_completo + ", nip=" + nip + ", no_tarjeta=" + no_tarjeta + ", fondos=" + fondos + '}';
    }
    
}
