/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cajero;

import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author J107-04
 */
public class Cajero {

    private String noTarjeta;
    private int nip;
    private Scanner teclado = new Scanner(System.in);
    private RegistroPersona db = new RegistroPersona();
    private Persona personaActual = new Persona();

    private boolean validarTarjeta(String numeroTarjeta) {

        for (Persona person : db.seleccionarPersonas()) {
            if (numeroTarjeta.equals(person.getNo_tarjeta())) {
                return true;
            }
        }
        return false;
    }

    private boolean validarUsuario(int nip_cur) {
        for (Persona person : db.seleccionarPersonas()) {
            if (person.getNip() == nip_cur) {
                if (this.noTarjeta.equals(person.getNo_tarjeta())) {
                    this.personaActual = person;
                    return true;
                }
                return false;
            }
        }
        return false;
    }

    private boolean validarFondos(double fondos) {
        return (this.personaActual.getFondos()) > fondos;
    }

    private double verFondos() {
        return this.personaActual.getFondos();
    }

    private void retirarFondos() {
        double fondos;
        boolean continuar = true;
        String conti = "s";
        boolean retiro_valido = false;

        while (!retiro_valido && continuar) {
            System.out.print("Ingrese la cantidad a retirar: ");
            fondos = teclado.nextDouble();
            if (this.validarFondos(fondos)) {
                retiro_valido = true;

                //Seteando fondos
                fondos = this.personaActual.getFondos() - fondos;
                db.actualizarFondos(this.personaActual, fondos);
                this.personaActual.setFondos(fondos);
            } else {
                System.out.println("Fondos insuficientes: Tus fondos => " + this.verFondos());
                System.out.println("Desea intentar de nuevo: (s|n)");
                conti = teclado.next();
                if (conti.toLowerCase().equals("s")) {
                    continuar = true;
                } else if (conti.toLowerCase().equals("n")) {
                    continuar = false;
                }
            }
        }

        System.out.println("Retiro realizado correctamente! ");
    }

    private void depositarFondos() {
        double fondos, fondos_finales;
        boolean continuar = true;
        boolean deposito_valido = false;
        String conti = "s";

        while (continuar) {
            System.out.print("Ingrese la cantidad a depositar: ");
            fondos = teclado.nextDouble();

            //Seteando fondos
            fondos_finales = this.personaActual.getFondos() + fondos;
            db.actualizarFondos(this.personaActual, fondos_finales);
            this.personaActual.setFondos(fondos_finales);
            System.out.println("Deposito realizado correctamente! ");
            System.out.println("¿Desea depositar mas dinero? (s|n) ");
            conti = teclado.next();
            if (conti.toLowerCase().equals("s")) {
                continuar = true;
            } else if (conti.toLowerCase().equals("n")) {
                continuar = false;
            }
        }

        
    }

    private boolean validarTransferencia(String beneficiario, double fondos) {

        if (this.validarTarjeta(beneficiario)) {
            if (this.validarFondos(fondos)) {
                return true;
            }
            return false;
        }
        return false;
    }

    private void transferirFondos() {
        double fondos, fondos_finales;
        Persona beneficiario = new Persona();
        Persona beneficiario_data;
        boolean transferencia_valida = false;
        String tarjeta_beneficiario;
        while (!transferencia_valida) {
            System.out.print("Ingrese el numero de tarjeta del beneficiario: ");
            tarjeta_beneficiario = teclado.next();
            System.out.print("Ingrese los fondos a transferir: ");
            fondos = teclado.nextDouble();
            System.out.println("Validando transferencia....");
            if (!this.personaActual.getNo_tarjeta().equals(tarjeta_beneficiario)) {
                if (this.validarTransferencia(tarjeta_beneficiario, fondos)) {
                    transferencia_valida = true;
                    //Seteando fondos
                    fondos_finales = this.personaActual.getFondos() - fondos;
                    //Obteniendo datos del beneficiario
                    beneficiario.setNo_tarjeta(tarjeta_beneficiario);
                    beneficiario_data = db.seleccionarPersona(beneficiario);

                    //Realizando transaccion de dinero
                    //Observador
                    db.actualizarFondos(this.personaActual, fondos_finales);
                    this.personaActual.setFondos(fondos_finales);

                    //Beneficiario
                    fondos_finales = beneficiario_data.getFondos() + fondos;
                    db.actualizarFondos(beneficiario_data, fondos_finales);

                } else {
                    System.out.println("La transferencia no puede ser completada :c");
                }
            } else {
                System.out.println("El beneficiario no puede ser tu propia tarjeta!");
            }

        }
        System.out.println("Transferencia realizada correctamente!");
    }

    public void run() throws InterruptedException {
        String modo;
        boolean continuar = true;
        while (continuar) {
            System.out.println("¿A QUE SECCION DESEA ACCEDER?\n\n A) Crear usuario        B) Cajero automatico \n\n ");
            System.out.print("Opcion: ");
            modo = teclado.next();
            if (modo.toLowerCase().equals("a")) {
                ejecutarRegistroUsuario();
                ejecutarCajero();
                continuar = false;
            } else if (modo.toLowerCase().equals("b")) {
                ejecutarCajero();
                continuar = false;
            } else {
                continuar = true;
            }
        }

        for (Persona person : db.seleccionarPersonas()) {
            System.out.println(person.toString());
        }
        //this.validarTransferencia("Martin",1000);
        System.out.println("\n\n Ejecucion finalizada");
    }

    private void ejecutarCajero() throws InterruptedException {
        String reload = "s";
        int intento = 1;

        boolean nip_valid = false;
        boolean tarjeta_valid = false;
        int option;
        System.out.println("BIENVENIDO, PORFAVOR INSERTE SU NUMERO DE TARJETA");

        while (!tarjeta_valid) {
            System.out.print("Numero de tarjeta: ");
            this.noTarjeta = teclado.next();
            System.out.println("Validando tarjeta....");
            if (this.validarTarjeta(this.noTarjeta)) {
                tarjeta_valid = true;
            }
            if (!tarjeta_valid) {
                System.out.println("La tarjeta no existe o ya no es valida, Intente de nuevo " + intento);
            }
            if (intento >= 5) {
                System.out.println("Has exedido el numero de intentos(" + intento + "), Abortando....");
                Thread.sleep(500);
                System.exit(0);
            }
            intento++;
        }
        intento = 1;
        while (!nip_valid) {
            System.out.print("Numero de NIP: ");
            try {
                this.nip = teclado.nextInt();
                System.out.println("Validando NIP....");
                if (this.validarUsuario(this.nip)) {
                    nip_valid = true;
                }
                if (!nip_valid) {
                    System.out.println("El numero de NIP no coincide con el numero de tarjeta, Intente nuevamente " + intento);
                }
                if (intento >= 5) {
                    System.out.println("Has exedido el numero de intentos(" + intento + "), Abortando....");
                    Thread.sleep(500);
                    System.exit(0);
                }
                intento++;
            } catch (InputMismatchException e) {
                System.out.println("Solo puedes introducir numeros");
                System.exit(0);
            }
        }

        while (reload.toLowerCase().equals("s")) {
            System.out.println("##########################################################################");
            System.out.println("Acceso correcto\n");
            System.out.println("Usuario => " + this.personaActual.getNombre_completo());
            System.out.println("\n##########################################################################");
            System.out.println("\n(1)Ver fondos  (2)Retirar fondos   (3)Depositar fondos     (4)Transferir fondos   (5)Salir");
            System.out.println("\n##########################################################################");

            System.out.print("¿Que accion desea realizar? ");
            option = teclado.nextInt();

            switch (option) {
                case 1:
                    System.out.println("Fondos => " + this.verFondos());
                    break;
                case 2:
                    this.retirarFondos();
                    break;
                case 3:
                    this.depositarFondos();
                    break;
                case 4:
                    this.transferirFondos();
                    break;
                case 5:
                    System.exit(0);
                    break;
                default:
                    System.exit(0);
            }

            System.out.print("¿Desea realizar otra accion? (s|n): ");
            reload = teclado.next();

        }

    }

    private void ejecutarRegistroUsuario() {
        String nombre, tarjeta;
        int nip;
        boolean nip_valid = false;
        boolean tarjeta_valid = false;

        Persona persona = new Persona();
        try {

            // Thread.sleep(300);
            while (!tarjeta_valid) {
                System.out.println("Ingrese los datos: ");
                System.out.println("nombre completo (Apellido, Nombre): ");
                nombre = teclado.next() + " " + teclado.next();
                //teclado.nextLine();
                persona.setNombre_completo(nombre);
                System.out.println("Numero de tarjeta: ");
                tarjeta = teclado.next();
                if (tarjeta.length() == 16) {
                    tarjeta_valid = true;
                    persona.setNo_tarjeta(tarjeta);
                } else {
                    System.out.println("El numero de tarjeta debe ser de 16 digitos");
                }
            }
            //  Thread.sleep(300);
            while (!nip_valid) {
                System.out.println("NIP: ");
                nip = teclado.nextInt();
                if (nip >= 1000 && nip <= 9999) {
                    nip_valid = true;
                    persona.setNip(nip);
                } else {
                    System.out.println("El nip debe contener solo 4 digitos");
                }
            }

            int id_cur = db.seleccionarPersonas().size();

            persona.setId(id_cur + 1);

            persona.setFondos(3000);

            db.insertarRegistro(persona);

            System.out.println("Usuario registrado correctamente");
        } catch (Exception e) {
            System.out.println("Ocurrio el siguiente error: " + e.getMessage());
        }

    }
}
