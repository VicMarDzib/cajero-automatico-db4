/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cajero;


import com.db4o.Db4oEmbedded;
import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author J107-04
 */
public class RegistroPersona {
    
    private ObjectContainer db = null;
    
    private void abrirRegistro()
    {
        db = Db4oEmbedded.openFile("cajeroAutomatico");
    }
    
    private void cerrarRegistro()
    {
        db.close();
    }
    
    public void insertarRegistro(Persona p){
        abrirRegistro();
        db.store(p);
        cerrarRegistro();
    }
    
    public List<Persona> seleccionarPersonas()
    {
        abrirRegistro();
        ObjectSet listaPersonas = db.queryByExample(Persona.class);
        List<Persona> lp = new ArrayList<>();
        for (Object persona : listaPersonas) {
            lp.add((Persona) persona);
        }
        cerrarRegistro();
        
        return lp;
    }
    
    public Persona seleccionarPersona(Persona p)
    {
        abrirRegistro();
        ObjectSet resultado = db.queryByExample(p);
        Persona persona = (Persona) resultado.next();
        cerrarRegistro();
        return persona;
    }
    

    public void eliminarPersona(int id)
    {
        abrirRegistro(); //abrir registro
        Persona persona = new Persona(); //objeto persona
        persona.setId(id); //Setear id
        ObjectSet resultado = db.queryByExample(persona); //resultado de consulta
        Persona preresultado = (Persona) resultado.next(); //asignar resultados de la bd
        db.delete(preresultado);
        System.out.println("Se elimino el registro correctamente");
    }

    public void actualizarFondos(Persona p, double newFondos)
    {
        abrirRegistro();
        ObjectSet resultado = db.queryByExample(p);
        Persona persona = (Persona) resultado.next();
        
        persona.setFondos(newFondos);
        
        db.store(persona);
        
        cerrarRegistro();
        //return persona;
    }
}
